<?php
namespace App\Services;

use App\Models\AssignmentRequest;
use App\Models\Enrollment;
use Illuminate\Support\Facades\DB;

class EnrollmentService {
    public function __construct() {

    }

    public function  getAll() {

            $enrollment = DB::table('Enrollment')->select(
                'TraineeID',
                'Trainee.Name',     
                'Class.ClassID',
                'Class.ClassName'
              
            )->join('Class', 'Class.ClassID', '=', 'Enrollment.ClassID')
            ->join('Trainee','Trainee.UserName','=','Enrollment.TraineeID')
            ->get();
    
            return json_encode($enrollment);
    }
        
            
    public function searchEnrollment($data) {
        $question =  DB::table('Enrollment')->select(
            'TraineeID',
            'Trainee.Name',     
            'Class.ClassID',
            'Class.ClassName'
        )
            
        ->join('Class', 'Class.ClassID', '=', 'Enrollment.ClassID')
        ->join('Trainee','Trainee.UserName','=','Enrollment.TraineeID')
        ->where('Class.ClassName','=',$data['ClassName'])
        ->orderBy('Class.ClassID')
        ->get();

        return json_encode($question);
    }

    public function information($data) {
        $question =  DB::table('Enrollment')->select(
            'TraineeID',
            'Trainee.Phone',
            'Trainee.Name',   
            'Trainee.Address',   
            'Trainee.Email',   
            'Class.ClassID',
            'Class.StartTime',
            'Class.ClassName',
            'Class.EndTime',
            'Class.Capacity',
            
        )
            
        ->join('Class', 'Class.ClassID', '=', 'Enrollment.ClassID')
        ->join('Trainee','Trainee.UserName','=','Enrollment.TraineeID')
        ->where('Class.ClassID','=',$data['ClassID'])
        ->where('TraineeID','=',$data['TraineeID'])
        ->get();

        return json_encode($question);
    }
}