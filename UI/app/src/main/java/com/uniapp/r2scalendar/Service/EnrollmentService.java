package com.uniapp.r2scalendar.Service;

import android.util.Log;
import android.view.View;

import com.uniapp.r2scalendar.Controller.IAssignmentController;
import com.uniapp.r2scalendar.Controller.IEnrollmentController;
import com.uniapp.r2scalendar.Model.ClassResponse;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.Model.QuestionResponse;
import com.uniapp.r2scalendar.Model.Topic;
import com.uniapp.r2scalendar.Repository.ClassRepository;
import com.uniapp.r2scalendar.Repository.EnrollmentRepository;
import com.uniapp.r2scalendar.Repository.QuestionResponsitory;
import com.uniapp.r2scalendar.Repository.TopicRepository;
import com.uniapp.r2scalendar.Utils.RetrofitClient;
import com.uniapp.r2scalendar.View.IClassView;
import com.uniapp.r2scalendar.View.IEnrollmentView;
import com.uniapp.r2scalendar.View.IInformationTraineeView;
import com.uniapp.r2scalendar.View.IQuestionView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnrollmentService {
    private IEnrollmentController enrollmentController;
    private IEnrollmentView enrollmentView;
    private View view;

    IInformationTraineeView informationTraineeView;

    public EnrollmentService(IEnrollmentView enrollmentView, View view, IEnrollmentController iEnrollmentController){
        this.enrollmentView = enrollmentView;
        this.view = view;
        this.enrollmentController = iEnrollmentController;
    }

    public EnrollmentService( IInformationTraineeView informationTraineeView, View view,IEnrollmentController iEnrollmentController){
        this.informationTraineeView = informationTraineeView;
        this.view = view;
        this.enrollmentController = iEnrollmentController;
    }

    public void getAllEnrollment() {
        try {
            EnrollmentRepository enrollmentRepository = RetrofitClient.Client().create(EnrollmentRepository.class);
            Call<List<EnrollmentResponse>> listCall = enrollmentRepository.getAll();
            listCall.enqueue(new Callback<List<EnrollmentResponse>>() {
                @Override
                public void onResponse(Call<List<EnrollmentResponse>> call, Response<List<EnrollmentResponse>> response) {
                    Log.e("Response Status", String.valueOf(response.code()));

                    enrollmentView.displayItem(view, response.body());

                }

                @Override
                public void onFailure(Call<List<EnrollmentResponse>> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchClass(String className) {
        try {
            if(className.equals("all"))
            {
                getAllEnrollment();
            }
            else {

                EnrollmentRepository enrollmentRepository = RetrofitClient.Client().create(EnrollmentRepository.class);
                Call<List<EnrollmentResponse>> enrollmentResposeCall = enrollmentRepository.searchClass(className);
                enrollmentResposeCall.enqueue(new Callback<List<EnrollmentResponse>>() {

                    @Override
                    public void onResponse(Call<List<EnrollmentResponse>> call, Response<List<EnrollmentResponse>> response) {
                        Log.e("Response Status", String.valueOf(response.code()));

                        enrollmentController.displayAllItem(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<EnrollmentResponse>> call, Throwable t) {

                        enrollmentController.onFailureResponse(t.getMessage());
                    }
                });
            }

        } catch(Exception e){
            e.printStackTrace();
        }

    }

    public  void getInformationTrainee(String classID, String traineeID)
    {
    try {

        EnrollmentRepository enrollmentRepository = RetrofitClient.Client().create(EnrollmentRepository.class);
        Call<List<EnrollmentResponse>> enrollmentResposeCall = enrollmentRepository.info(Integer.parseInt(classID),traineeID);
        enrollmentResposeCall.enqueue(new Callback<List<EnrollmentResponse>>() {

            @Override
            public void onResponse(Call<List<EnrollmentResponse>> call, Response<List<EnrollmentResponse>> response) {
                Log.e("Response Status", String.valueOf(response.code()));

                enrollmentController.displayAllItem1(response.body());

            }

            @Override
            public void onFailure(Call<List<EnrollmentResponse>> call, Throwable t) {
            }
        });

    } catch(Exception e){
        e.printStackTrace();
    }

}

    public void getAll(IEnrollmentView enrollmentView, View view) {
        try {
            ClassRepository classRepository = RetrofitClient.Client().create(ClassRepository.class);
            Call<List<ClassResponse>> listCall = classRepository.getAll();
            listCall.enqueue(new Callback<List<ClassResponse>>() {
                @Override
                public void onResponse(Call<List<ClassResponse>> call, Response<List<ClassResponse>> response) {
                    Log.e("Response Status", String.valueOf(response.code()));

                    {
                        enrollmentView.loadSpinner(response.body(),view);

                    }

                }

                @Override
                public void onFailure(Call<List<ClassResponse>> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
