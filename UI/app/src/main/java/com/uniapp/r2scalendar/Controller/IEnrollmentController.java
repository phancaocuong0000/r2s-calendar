package com.uniapp.r2scalendar.Controller;

import android.view.View;

import com.uniapp.r2scalendar.Model.ClassResponse;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.View.IAddQuestionView;

import java.util.List;
import java.util.Map;

public interface IEnrollmentController {
    void getAll();
    void displayAllItem(List<EnrollmentResponse> enrollmentResponseList);
    void loadSpinnerClass();
    void setSpinnerClassDashboard(List<ClassResponse> list);
    void searchClass(Map<String,Object> params);
    void deleteQEnrollment(Map<String,Object> params);
    void onFailureResponse(String message);
    void getInformation(Map<String,Object> params);
    void displayAllItem1(List<EnrollmentResponse> enrollmentResponseList);

    void getList();

}
