package com.uniapp.r2scalendar.ViewHolder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uniapp.r2scalendar.R;

public class EnrollmentViewHolder extends RecyclerView.ViewHolder {
    public TextView traineeID,traineeName,classID,className;
    public  ImageButton edit,delete,showDetail;

    public EnrollmentViewHolder(@NonNull View itemView) {
        super(itemView);

        traineeID = itemView.findViewById(R.id.tvETraineeID);
        traineeName = itemView.findViewById(R.id.tvETraineeName);
        classID = itemView.findViewById(R.id.tvEClassID);
        className = itemView.findViewById(R.id.tvEClassName);

        edit = itemView.findViewById(R.id.imageEEdit);
        delete = itemView.findViewById(R.id.imageEDelete);
        showDetail = itemView.findViewById(R.id.imageEDetail);
    }
}
