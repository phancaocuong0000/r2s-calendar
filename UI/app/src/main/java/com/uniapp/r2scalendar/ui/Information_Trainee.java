package com.uniapp.r2scalendar.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uniapp.r2scalendar.Controller.EnrollmentController;
import com.uniapp.r2scalendar.Controller.IEnrollmentController;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.R;
import com.uniapp.r2scalendar.View.IEnrollmentView;
import com.uniapp.r2scalendar.View.IInformationTraineeView;
import com.uniapp.r2scalendar.adapter.EnrollmentAdapter;
import com.uniapp.r2scalendar.adapter.InformationTraineeAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Information_Trainee extends Fragment implements IInformationTraineeView {

    IEnrollmentController enrollmentController;
    List<EnrollmentResponse> enrollmentResponseList;
    EnrollmentResponse enrollmentResponse;

    public TextView traineeID,phone,traineeName,address,email;
    public TextView classID,startTime,className,endTime,capacity;
    Button buttonBack;
    View v;
    ProgressDialog progressDialog;

    public Information_Trainee(EnrollmentResponse enrollmentResponse) {
        this.enrollmentResponse=enrollmentResponse;
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.information, container, false);
    }
        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

            enrollmentController=new EnrollmentController(this,view);

            progressDialog = new android.app.ProgressDialog(view.getContext());
            progressDialog.setTitle("Please wait...");
            progressDialog.setMessage("Data loading...");
            progressDialog.show();

            initVariable(view);
            handleItem();
        }

    private void handleItem()
    {
        Map<String, Object> params = new HashMap<>();
        params.put("classID", enrollmentResponse.getClassID());
        params.put("traineeID", enrollmentResponse.getTraineeID());
        enrollmentController.getInformation(params);
    }


    @Override
    public void displayItem(View v, List<EnrollmentResponse> enrollmentResponseList) {

    }

    @Override
    public void initVariable(View v) {

        traineeID = v.findViewById(R.id.tveTraineeID);
        phone = v.findViewById(R.id.txtephone);
        traineeName = v.findViewById(R.id.tveTraineeName);
        address = v.findViewById(R.id.txteAddress);
        email = v.findViewById(R.id.txteemail);


        classID = v.findViewById(R.id.txteclassID);
        startTime = v.findViewById(R.id.txteStartTime);
        className = v.findViewById(R.id.txteClassName);
        endTime = v.findViewById(R.id.txteEndTime);
        capacity = v.findViewById(R.id.txteCapacity);

        buttonBack = v.findViewById(R.id.btnBackInfo);

        buttonBack.setOnClickListener(t->{
            GlobalFragment.replaceFragment(new EnrollmentFragment(), getActivity());
        });

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void Information_Trainee(List<EnrollmentResponse> enrollmentResponseList) {
        this.enrollmentResponseList=enrollmentResponseList;
        //Information_Trainee(enrollmentResponseList);
        traineeID.setText("Trairne ID: " + enrollmentResponseList.get(0).getTraineeID());
        phone.setText("Phone: " + enrollmentResponseList.get(0).getPhone());
        traineeName.setText("Trainee Name: " + enrollmentResponseList.get(0).getName());
        address.setText("Address: " +enrollmentResponseList.get(0).getAddress());
        email.setText("Email: " + enrollmentResponseList.get(0).getEmail());

        startTime.setText("StartTime: " + enrollmentResponseList.get(0).getStartTime());
        className.setText("Class Name: " + enrollmentResponseList.get(0).getClassName());
        endTime.setText("EndTime: " + enrollmentResponseList.get(0).getEndTime());
        capacity.setText("Capacity: " +enrollmentResponseList.get(0).getCapacity());
        classID.setText("Class ID: " + enrollmentResponseList.get(0).getClassID());

        progressDialog.dismiss();
    }

    @Override
    public void displayProgressDialog(View view) {
        progressDialog = new android.app.ProgressDialog(view.getContext());
        progressDialog.setTitle("Please wait...");
        progressDialog.setMessage("Question is loading...");
        progressDialog.show();
    }

    @Override
    public void disableProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
    }

}


