package com.uniapp.r2scalendar.View;

import android.view.View;

import com.uniapp.r2scalendar.Model.MessageResponse;

public interface iAddEnrollmentView {
    void onSuccess(String message, MessageResponse messageResponse);
    void displayProgressDialog(View view);
    void disableProgressDialog();
}
