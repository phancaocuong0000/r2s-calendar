package com.uniapp.r2scalendar.Model;

public class EnrollmentResponse {

    private String TraineeID;
    private String Name;

    public EnrollmentResponse(String traineeID, String name, int classID, String className, int capacity, String startTime, String endTime, String email, String phone, String address) {
        TraineeID = traineeID;
        Name = name;
        ClassID = classID;
        ClassName = className;
        Capacity = capacity;
        StartTime = startTime;
        EndTime = endTime;
        Email = email;
        Phone = phone;
        Address = address;
    }

    private int ClassID;
    private String ClassName;
    private int Capacity;
    private String StartTime;
    private String EndTime;
    private String Email;
    private String Phone;
    private String Address;

    public EnrollmentResponse(int classID, String traineeID) {
        TraineeID = traineeID;
        ClassID = classID;
    }

    public int getCapacity() {
        return Capacity;
    }

    public void setCapacity(int capacity) {
        Capacity = capacity;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }



    public EnrollmentResponse(String traineeID, String name, int classID, String className) {
        TraineeID = traineeID;
        Name = name;
        ClassID = classID;
        ClassName = className;
    }

    public String getTraineeID() {
        return TraineeID;
    }

    public void setTraineeID(String traineeID) {
        TraineeID = traineeID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }














}
