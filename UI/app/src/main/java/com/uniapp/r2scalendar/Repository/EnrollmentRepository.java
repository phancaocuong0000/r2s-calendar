package com.uniapp.r2scalendar.Repository;

import com.uniapp.r2scalendar.Model.ClassResponse;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.Model.QuestionResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EnrollmentRepository {
    @GET("enrollment")
    Call<List<EnrollmentResponse>> getAll();

    @GET("enrollment/search")
    Call<List<EnrollmentResponse>> searchClass(@Query("ClassName") String className);

    @GET("enrollment/information")
    Call<List<EnrollmentResponse>> info(@Query("ClassID") int classID,@Query("TraineeID") String traineeID);
}
