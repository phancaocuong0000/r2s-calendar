package com.uniapp.r2scalendar.Controller;

import android.view.View;

import com.uniapp.r2scalendar.Model.ClassResponse;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.Service.ClassService;
import com.uniapp.r2scalendar.Service.EnrollmentService;
import com.uniapp.r2scalendar.View.IEnrollmentView;
import com.uniapp.r2scalendar.View.IInformationTraineeView;

import java.util.List;
import java.util.Map;

public class EnrollmentController implements IEnrollmentController {
    IEnrollmentView enrollmentView;
    View view;
    EnrollmentService enrollmentService;
    IInformationTraineeView informationTraineeView;

    public EnrollmentController( IInformationTraineeView informationTraineeView, View view) {
        this.informationTraineeView = informationTraineeView;
        this.view = view;
        enrollmentService = new EnrollmentService(informationTraineeView, view, this);
    }

    public EnrollmentController( IEnrollmentView enrollmentView, View view) {
        this.enrollmentView = enrollmentView;
        this.view = view;
        enrollmentService = new EnrollmentService(enrollmentView, view, this);
    }
    @Override
    public void displayAllItem(List<EnrollmentResponse> enrollmentResponseList) {

        enrollmentView.displayItem(view,enrollmentResponseList);

    }

    @Override
    public void getAll()
    {

        enrollmentService.getAllEnrollment();
    }

    @Override
    public void loadSpinnerClass() {
        ClassService classService = new ClassService(this,view);
        classService.getAllClass(true);
    }

    @Override
    public void setSpinnerClassDashboard(List<ClassResponse> list) {
        enrollmentView.loadSpinner(list,view);
    }


    @Override
    public void searchClass(Map<String, Object> params) {

        enrollmentService.searchClass((String) params.get("className"));

    }

    @Override
    public void deleteQEnrollment(Map<String, Object> params) {

    }

    @Override
    public void onFailureResponse(String message) {

    }

    @Override
    public void getInformation(Map<String,Object> params) {

        enrollmentService.getInformationTrainee( params.get("classID").toString(), params.get("traineeID").toString());

    }

    @Override
    public void displayAllItem1(List<EnrollmentResponse> enrollmentResponseList) {
        informationTraineeView.Information_Trainee(enrollmentResponseList);
    }

    @Override
    public void getList()
    {
        enrollmentService.getAll( enrollmentView,view);
    }


}
