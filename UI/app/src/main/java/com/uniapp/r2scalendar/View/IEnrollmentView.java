package com.uniapp.r2scalendar.View;

import android.view.View;

import com.uniapp.r2scalendar.Model.ClassResponse;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.Model.Topic;

import java.util.List;

public interface IEnrollmentView {
    void displayItem(View v, List<EnrollmentResponse> enrollmentResponseList);
    void onSuccess(String message);
    void loadSpinner(List<ClassResponse> classResponseList, View view);
}
