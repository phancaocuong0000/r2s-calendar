package com.uniapp.r2scalendar.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uniapp.r2scalendar.Controller.ClassController;
import com.uniapp.r2scalendar.Controller.EnrollmentController;
import com.uniapp.r2scalendar.Controller.IEnrollmentController;
import com.uniapp.r2scalendar.Controller.QuestionController;
import com.uniapp.r2scalendar.Model.ClassResponse;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.Model.Topic;
import com.uniapp.r2scalendar.R;
import com.uniapp.r2scalendar.View.IEnrollmentView;
import com.uniapp.r2scalendar.adapter.ClassSpineerAdapter;
import com.uniapp.r2scalendar.adapter.EnrollmentAdapter;
import com.uniapp.r2scalendar.adapter.TopicSpinnerAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnrollmentFragment  extends Fragment implements IEnrollmentView {

    IEnrollmentController enrollmentController;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    EnrollmentAdapter enrollmentAdapter;
    ProgressDialog progressDialog;
    Spinner spinnerClassName;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_enrollment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        enrollmentController = new EnrollmentController(this, view);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Please wait...");
        progressDialog.setMessage("Loading data.......");
        progressDialog.show();

        initVariables(view);

        enrollmentController.getAll();
        enrollmentController.getList();

        spinnerClassName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Your code here
                Map<String, Object> params = new HashMap<>();
                params.put("className", ((ClassResponse) spinnerClassName.getSelectedItem()).getClassName());

               enrollmentController.searchClass(params);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });




    }

    @Override
    public void displayItem(View v, List<EnrollmentResponse> enrollmentResponseList) {

        enrollmentAdapter =new EnrollmentAdapter(enrollmentResponseList, v, enrollmentController, getActivity(), this);
        recyclerView.setAdapter(enrollmentAdapter);
        enrollmentAdapter.notifyDataSetChanged();
        progressDialog.dismiss();
    }


    @Override
    public void onSuccess(String message) {

        Dialog dialog = new Dialog(this.getContext());
        dialog.setContentView(R.layout.dialog_1_button);
        dialog.setCancelable(false);
        ((TextView) dialog.findViewById(R.id.tvContent)).setText(message);
        dialog.findViewById(R.id.btOK).setOnClickListener(v -> {
            dialog.dismiss();
            GlobalFragment.replaceFragment(new EnrollmentFragment(),getActivity());
        });
        dialog.show();

    }



    private void initVariables(View v) {

        spinnerClassName=v.findViewById(R.id.spinnerEClassName);
        recyclerView = v.findViewById(R.id.rvEnrollment);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(v.getContext());
        recyclerView.setLayoutManager(layoutManager);
        registerForContextMenu(recyclerView);
    }

    @Override
    public void loadSpinner(List<ClassResponse> classResponseList, View view) {
        ClassSpineerAdapter topicSpinnerAdapter = new ClassSpineerAdapter(this.getContext(), android.R.layout.simple_spinner_item, classResponseList);
        spinnerClassName.setAdapter(topicSpinnerAdapter);

    }
}
