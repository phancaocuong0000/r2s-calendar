package com.uniapp.r2scalendar.adapter;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.uniapp.r2scalendar.Controller.IEnrollmentController;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.R;
import com.uniapp.r2scalendar.View.IEnrollmentView;
import com.uniapp.r2scalendar.ViewHolder.EnrollmentViewHolder;
import com.uniapp.r2scalendar.ui.GlobalFragment;
import com.uniapp.r2scalendar.ui.Information_Trainee;

import java.util.List;

public class EnrollmentAdapter extends RecyclerView.Adapter<EnrollmentViewHolder> {

    List<EnrollmentResponse> enrollmentResponseList;
    IEnrollmentController enrollmentController;
    View view;
    FragmentActivity fragmentActivity;
    IEnrollmentView enrollmentView;

    public EnrollmentAdapter(List<EnrollmentResponse> enrollmentResponseList, View view,  IEnrollmentController enrollmentController,  FragmentActivity fragmentActivity,  IEnrollmentView enrollmentView) {
        this.enrollmentResponseList = enrollmentResponseList;
        this.view = view;
        this.enrollmentController = enrollmentController;
        this.fragmentActivity = fragmentActivity;
        this.enrollmentView = enrollmentView;
    }
    @NonNull
    @Override
    public EnrollmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(view.getContext())
                .inflate(R.layout.item_enrollment, parent, false);
        return new EnrollmentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EnrollmentViewHolder holder, int position) {

        holder.traineeID.setText("Trairne ID: " + enrollmentResponseList.get(position).getTraineeID());
        holder.traineeName.setText("Trainee Name: " + enrollmentResponseList.get(position).getName());
        holder.classID.setText("Class ID: " + enrollmentResponseList.get(position).getClassID());
        holder.className.setText("Class Name: " + enrollmentResponseList.get(position).getClassName());

        holder.delete.setOnClickListener(v -> {

        });

        holder.edit.setOnClickListener(t -> {

        });
        holder.showDetail.setOnClickListener(s->{

            EnrollmentResponse enrollmentResponse=new EnrollmentResponse(enrollmentResponseList.get(position).getClassID(),
                    enrollmentResponseList.get(position).getTraineeID());

            GlobalFragment.replaceFragment(new Information_Trainee(enrollmentResponse), fragmentActivity);


        });

    }

    @Override
    public int getItemCount() {
        return enrollmentResponseList.size();
    }

    public void confirmPopup(String title, String message, View view, int position) {
        Dialog dialog = new Dialog(view.getContext());
        dialog.setContentView(R.layout.dialog_2_button);
        dialog.setCancelable(false);

        ((TextView) dialog.findViewById(R.id.tvContent)).setText(title);
        ((TextView) dialog.findViewById(R.id.btCancel)).setText("Cancel");
        ((TextView) dialog.findViewById(R.id.tvContent3)).setText(message);
        dialog.findViewById(R.id.btOK).setOnClickListener(t -> {
            dialog.dismiss();
         //   enrollmentController.handleDeleteClassItem(String.valueOf(classResponseList.get(position).getClassID()), classView, view);
        });

        dialog.findViewById(R.id.btCancel).setOnClickListener(t -> {
            dialog.dismiss();
        });
        dialog.show();
    }
}
