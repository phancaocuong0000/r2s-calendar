package com.uniapp.r2scalendar.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.uniapp.r2scalendar.Controller.IEnrollmentController;
import com.uniapp.r2scalendar.Model.EnrollmentResponse;
import com.uniapp.r2scalendar.Model.QuestionResponse;
import com.uniapp.r2scalendar.R;
import com.uniapp.r2scalendar.View.IEnrollmentView;
import com.uniapp.r2scalendar.View.IInformationTraineeView;
import com.uniapp.r2scalendar.ViewHolder.EnrollmentViewHolder;
import com.uniapp.r2scalendar.ViewHolder.InformationTraineeViewHolder;
import com.uniapp.r2scalendar.ui.AddQuestionFragment;
import com.uniapp.r2scalendar.ui.EnrollmentFragment;
import com.uniapp.r2scalendar.ui.GlobalFragment;
import com.uniapp.r2scalendar.ui.Information_Trainee;

import java.util.List;


public class InformationTraineeAdapter extends RecyclerView.Adapter<InformationTraineeViewHolder> {

    List<EnrollmentResponse> enrollmentResponseList;
    IEnrollmentController enrollmentController;
    View view;
    FragmentActivity fragmentActivity;
    IInformationTraineeView informationTraineeView;

    public InformationTraineeAdapter(List<EnrollmentResponse> enrollmentResponseList, View view,  IEnrollmentController enrollmentController,  FragmentActivity fragmentActivity,  IInformationTraineeView informationTraineeView) {
        this.enrollmentResponseList = enrollmentResponseList;
        this.view = view;
        this.enrollmentController = enrollmentController;
        this.fragmentActivity = fragmentActivity;
        this.informationTraineeView = informationTraineeView;
    }

    @NonNull
    @Override
    public InformationTraineeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(view.getContext())
                .inflate(R.layout.item_detail_trainee, parent, false);
        return new InformationTraineeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InformationTraineeViewHolder holder, int position) {

        holder.traineeID.setText("Trairne ID: " + enrollmentResponseList.get(position).getTraineeID());
        holder.phone.setText("Phone: " + enrollmentResponseList.get(position).getPhone());
        holder.traineeName.setText("Trainee Name: " + enrollmentResponseList.get(position).getName());
        holder.address.setText("Address: " + enrollmentResponseList.get(position).getAddress());
        holder.email.setText("Email: " + enrollmentResponseList.get(position).getEmail());

        holder.classID.setText("Class ID: " + enrollmentResponseList.get(position).getClassID());
        holder.startTime.setText("StartTime: " + enrollmentResponseList.get(position).getStartTime());
        holder.className.setText("Class Name: " + enrollmentResponseList.get(position).getClassName());
        holder.endTime.setText("EndTime: " + enrollmentResponseList.get(position).getEndTime());
        holder.capacity.setText("Capacity: " + enrollmentResponseList.get(position).getCapacity());



    }

    @Override
    public int getItemCount() {
        return enrollmentResponseList.size();
    }
}

