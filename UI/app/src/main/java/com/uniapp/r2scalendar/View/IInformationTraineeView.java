package com.uniapp.r2scalendar.View;

import android.view.View;

import com.uniapp.r2scalendar.Model.EnrollmentResponse;

import java.util.List;

public interface IInformationTraineeView {

    void displayItem(View v, List<EnrollmentResponse> enrollmentResponseList);
    void initVariable(View v);
    void Information_Trainee(List<EnrollmentResponse> enrollmentResponseList);
    void displayProgressDialog(View view);
    void disableProgressDialog();
}
