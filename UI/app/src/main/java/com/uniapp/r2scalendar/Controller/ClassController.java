package com.uniapp.r2scalendar.Controller;

import android.view.View;

import androidx.annotation.NonNull;

import com.uniapp.r2scalendar.Model.ClassResponse;
import com.uniapp.r2scalendar.Service.ClassService;
import com.uniapp.r2scalendar.View.IAddClassView;
import com.uniapp.r2scalendar.View.IClassView;
import com.uniapp.r2scalendar.View.IEnrollmentView;

public class ClassController implements IClassController {
    IClassView classView;
    View view;
    ClassService classService;

    IEnrollmentView enrollmentView;

    public ClassController(IEnrollmentView enrollmentView, View view) {
        this.enrollmentView = enrollmentView;
        this.view = view;
        classService = new ClassService(classView, view);
    }

    public ClassController(IClassView classView, View view) {
        this.classView = classView;
        this.view = view;
        classService = new ClassService(classView, view);
    }
    @Override
    public void displayAllItem() {
        classView.displayProgressDialog(view);
        classService.getAllClass(false);
    }

    @Override
    public void handleClassItem(ClassResponse classResponse, IAddClassView addClassView, View view) {
        addClassView.displayProgressDialog(view);
        if (classResponse.getClassID() != 0) {
            classResponse.setStartTime(classResponse.getStartTime().replace("/", "-"));
            classResponse.setEndTime(classResponse.getEndTime().replace("/", "-"));

            classService.handleEditClass(classResponse, addClassView);
        } else
             classService.handleAddClass(classResponse, addClassView);
    }

    @Override
    public void handleDeleteClassItem(String ClassID, IClassView classView, View view) {
        classView.displayProgressDialog(view);
        classService.handleDeleteClass(ClassID, classView);
    }


}
